package com.cognizant.springlearn1.dao;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.springlearn1.Employee;
import com.cognizant.springlearn1.exception.EmployeeNotFoundException;

@Component
public class EmployeeDao {

	private static ArrayList<Employee> employeeList;

	public EmployeeDao() {
		ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
		this.employeeList = context.getBean("employeeList", ArrayList.class);
	}

	public ArrayList<Employee> getAllEmployees() {
		return employeeList;
	}

	public Employee updateEmployee(Employee employee) throws EmployeeNotFoundException {
		for(Employee employee2 : employeeList) {
			if(employee2.getEmployeeId() == employee.getEmployeeId()) {
				employee2.setDepartment(employee.getDepartment());
				employee2.setEmployeeName(employee.getEmployeeName());
				return employee2;
			}
		}
		throw new EmployeeNotFoundException();
	} 
	
	public void deleteEmployee(int id) throws EmployeeNotFoundException {
		Employee employee3 = null;
		for(Employee employee2 : employeeList) {
			if(employee2.getEmployeeId() == id) {
				employee3 = employee2;
				break ;
			}
		}
		if(employee3 != null) {
			employeeList.remove(employee3);
			return;
		}
		throw new EmployeeNotFoundException();
	}
}
