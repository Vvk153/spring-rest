package com.cognizant.springlearn1.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.springlearn1.Employee;
import com.cognizant.springlearn1.dao.EmployeeDao;
import com.cognizant.springlearn1.exception.EmployeeNotFoundException;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	
	//@Transaction
	public ArrayList<Employee> getAllEmployees() {
		
		return employeeDao.getAllEmployees();
	}
	
	public Employee updateEmployee(Employee employee) throws EmployeeNotFoundException {
		return employeeDao.updateEmployee(employee);
	}
	
	public void deleteEmployee(int id) throws EmployeeNotFoundException {
		employeeDao.deleteEmployee(id);
	}
}
