package com.cognizant.springlearn1.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.springlearn1.Department;
import com.cognizant.springlearn1.dao.DepartmentDao;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentDao departmentDao;
	
	public ArrayList<Department> getAllDepartments() {
		return departmentDao.getAllDepartments();
	}
}
