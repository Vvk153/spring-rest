package com.cognizant.springlearn.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearn.Country;
import com.cognizant.springlearn.service.countryService;
import com.cognizant.springlearn.service.exception.CountryNotFoundException;

@RestController
public class CountryController {

	@Autowired
	private countryService countryService;

	private ApplicationContext context;

	@GetMapping("/country")
	public Country getCountryIndia() {
		context = new ClassPathXmlApplicationContext("country.xml");
		Country india = (Country) context.getBean("in", Country.class);
		return india;
	}

	@GetMapping("/countries")
	public ArrayList<Country> getAllCountries() {
		context = new ClassPathXmlApplicationContext("country.xml");
		ArrayList<Country> countryList = context.getBean("countryList", ArrayList.class);
		return countryList;
	}

	@GetMapping("/country/{code}")
	public Country getCountry(@PathVariable String code) throws CountryNotFoundException {
		Country country = countryService.getCountry(code);
		return country;
	}
}
