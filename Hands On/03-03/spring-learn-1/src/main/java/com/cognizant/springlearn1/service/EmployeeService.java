package com.cognizant.springlearn1.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.springlearn1.Employee;
import com.cognizant.springlearn1.dao.EmployeeDao;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	
	//@Transaction
	public ArrayList<Employee> getAllEmployees() {
		
		return employeeDao.getAllEmployees();
	}
}
