package com.cognizant.springlearn1.dao;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.springlearn1.Employee;

@Component
public class EmployeeDao {

	private static ArrayList<Employee> employeeList;

	public EmployeeDao() {
		ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
		this.employeeList = context.getBean("employeeList", ArrayList.class);
	}

	public ArrayList<Employee> getAllEmployees() {
		return employeeList;
	}

}
