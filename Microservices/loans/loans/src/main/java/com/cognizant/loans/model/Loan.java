package com.cognizant.loans.model;

public class Loan {

	private String loanNumber;
	private String loanType;
	private int loanAmount;
	private int loanEMI;
	private int loanTenure;

	public Loan() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Loan(String loanNumber, String loanType, int loanAmount, int loanEMI, int loanTenure) {
		super();
		this.loanNumber = loanNumber;
		this.loanType = loanType;
		this.loanAmount = loanAmount;
		this.loanEMI = loanEMI;
		this.loanTenure = loanTenure;
	}

	public String getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public int getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(int loanAmount) {
		this.loanAmount = loanAmount;
	}

	public int getLoanEmi() {
		return loanEMI;
	}

	public void setLoanEmi(int loanEmi) {
		this.loanEMI = loanEmi;
	}

	public int getLoanTenure() {
		return loanTenure;
	}

	public void setLoanTenure(int loanTenure) {
		this.loanTenure = loanTenure;
	}

	@Override
	public String toString() {
		return "Loan [loanNumber=" + loanNumber + ", loanType=" + loanType + ", loanAmount=" + loanAmount + ", loanEMI="
				+ loanEMI + ", loanTenure=" + loanTenure + "]";
	}

}
