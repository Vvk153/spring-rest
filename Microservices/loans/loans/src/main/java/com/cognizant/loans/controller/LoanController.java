package com.cognizant.loans.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.loans.model.Loan;

@RestController
public class LoanController {

	@GetMapping("/loans/{loanNumber}")
	public Loan getLoan(@PathVariable String loanNumber) {
		ApplicationContext context = new ClassPathXmlApplicationContext("loan.xml");
		Loan loan = context.getBean("loan", Loan.class);
		return loan;
	}

}
