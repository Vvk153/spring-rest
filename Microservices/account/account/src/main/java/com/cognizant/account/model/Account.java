package com.cognizant.account.model;

public class Account {

	private String accountNumber;
	private String accountType;
	private double accountBalance;

	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Account(String accountNumber, String accountType, double accountBalance) {
		super();
		this.accountNumber = accountNumber;
		this.accountType = accountType;
		this.accountBalance = accountBalance;
	}

	public String getAccnumber() {
		return accountNumber;
	}

	public void setAccnumber(String accnumber) {
		this.accountNumber = accnumber;
	}

	public String getAcctype() {
		return accountType;
	}

	public void setAcctype(String acctype) {
		this.accountType = acctype;
	}

	public double getAccbalance() {
		return accountBalance;
	}

	public void setAccbalance(double accbalance) {
		this.accountBalance = accbalance;
	}

	@Override
	public String toString() {
		return "Account [accountNumber=" + accountNumber + ", accountType=" + accountType + ", accountBalance="
				+ accountBalance + "]";
	}

}
