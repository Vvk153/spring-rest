package com.cognizant.account.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.account.model.Account;

@RestController
public class AccountController {

	@GetMapping("/accounts/{accnumber}")
	public Account getAccount(@PathVariable String accnumber) {
		List<Account> list = new ArrayList<Account>();
		ApplicationContext context = new ClassPathXmlApplicationContext("account.xml");
		Account account = context.getBean("account", Account.class);
		return account;
	}

}
