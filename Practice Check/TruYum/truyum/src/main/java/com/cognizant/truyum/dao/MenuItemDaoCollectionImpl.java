package com.cognizant.truyum.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.truyum.model.MenuItem;
import com.cognizant.truyum.util.DateUtil;

@Component
public class MenuItemDaoCollectionImpl implements MenuItemDao {

	private static List<MenuItem> menuItemList;

	ApplicationContext context;
	
	public MenuItemDaoCollectionImpl() {
		super();
	}

	@PostConstruct
	public void init() {
		context = new ClassPathXmlApplicationContext("truyum.xml");
		menuItemList = context.getBean("menuItem", ArrayList.class);
	} 
	
	@Override
	public List<MenuItem> getMenuItemListAdmin() {
		return menuItemList;
	}

	@Override
	public List<MenuItem> getMenuItemListCustomer() {
		ArrayList<MenuItem> menuItemsListCustomer = new ArrayList<MenuItem>();
		Date date = new Date();
		SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yy");
		date = DateUtil.convertToDate(sdfDate.format(date));

		for (MenuItem menuItem : menuItemList) {
			if (menuItem.getDateOfLaunch().compareTo(date) < 0 && menuItem.isActive()) {
				menuItemsListCustomer.add(menuItem);
			}
		}
		return menuItemsListCustomer;
	}

	@Override
	public void modifyMenuItem(MenuItem menuItem) {
		for (MenuItem menuItemLocal : menuItemList) {
			if (menuItemLocal.getId() == menuItem.getId()) {
				System.out.println("Match " + menuItemLocal.getId());
				menuItemLocal.setName(menuItem.getName());
				menuItemLocal.setPrice(menuItem.getPrice());
				menuItemLocal.setDateOfLaunch(menuItem.getDateOfLaunch());
				menuItemLocal.setCategory(menuItem.getCategory());
				menuItemLocal.setActive(menuItem.isActive());
				menuItemLocal.setFreeDelivery(menuItem.isFreeDelivery());
				return;
			}
		}
	}

	@Override
	public MenuItem getMenuItem(long menuItemId) {
		for (MenuItem menuItemLocal : menuItemList) {
			if (menuItemLocal.getId() == menuItemId) {
				return menuItemLocal;
			}
		}
		return null;
	}

}
