package com.cognizant.truyum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.dao.MenuItemDao;
import com.cognizant.truyum.model.MenuItem;

@Service
public class MenuItemService {

	@Autowired
	MenuItemDao menuItemDaoCollectionImpl;
	
	public List<MenuItem> getMenuItemListCustomer() {
		return menuItemDaoCollectionImpl.getMenuItemListAdmin();
	}
	
	public MenuItem getMenuItem(Long menuItemId) {
		return menuItemDaoCollectionImpl.getMenuItem(menuItemId);
	}
	
	public void modifyMenuItem(MenuItem menuItem) {
		menuItemDaoCollectionImpl.modifyMenuItem(menuItem);
	}
}
