package com.cognizant.truyum.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.annotation.PostConstruct;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.truyum.model.Cart;
import com.cognizant.truyum.model.MenuItem;

@Component
public class CartDaoCollectionImpl implements CartDao {

	private static HashMap<String, Cart> userCarts;

	public CartDaoCollectionImpl() {
		super();
		if (userCarts == null) {
			userCarts = new HashMap<String, Cart>();
		}
	}
	
//	@PostConstruct
//	public void init() {
//		ApplicationContext context = new ClassPathXmlApplicationContext("truyum.xml");
//		ArrayList<MenuItem> menuItemList = context.getBean("menuItem", ArrayList.class);
//		userCarts.put("1", new Cart(menuItemList, 100));
//		System.out.println("Init");
//	}

	@Override
	public void addCartItem(long userId, long menuItemId) {
		MenuItemDao menuItemDao = new MenuItemDaoCollectionImpl();
		MenuItem menuItem = menuItemDao.getMenuItem(menuItemId);
		if (userCarts.containsKey(Long.toString(userId))) {
			Cart cart = userCarts.get(Long.toString(userId));
			List<MenuItem> menuItemsList = cart.getMenuItemList();
			menuItemsList.add(menuItem);
		} else {
			Cart cart = new Cart();
			List<MenuItem> menuItemList = new ArrayList<MenuItem>();
			menuItemList.add(menuItem);
			cart.setMenuItemList(menuItemList);
			userCarts.put(Long.toString(userId), cart);

		}
	}

	@Override
	public void removeCartItem(long userId, long menuItemId) {

		Cart cart = userCarts.get(Long.toString(userId));
		ListIterator<MenuItem> iterator = cart.getMenuItemList().listIterator();
		while (iterator.hasNext()) {
			if (iterator.next().getId() == menuItemId) {
				iterator.remove();
			}
		}

	}

	// There is an issue in class diagram it is mentioned List<MenuItem> as
	// return type
	// But in description it is mentioned as Cart type
	@Override
	public List<MenuItem> getAllCartItems(long userId) throws CartEmptyException {
		Cart cart = userCarts.get(Long.toString(userId));
		List<MenuItem> menuItemsList = cart.getMenuItemList();
		if (menuItemsList.isEmpty()) {
			throw new CartEmptyException();
		}
		double total = 0;
		for (MenuItem menuItem : menuItemsList) {
			total += menuItem.getPrice();
		}
		cart.setTotal(total);
		return cart.getMenuItemList();
	}

}
