package com.cognizant.truyum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.dao.CartDao;
import com.cognizant.truyum.dao.CartEmptyException;
import com.cognizant.truyum.model.Cart;
import com.cognizant.truyum.model.MenuItem;

@Service
public class CartService {

	@Autowired
	private CartDao cartDaoCollectionImpl;
	
	public void addCartItem(Long userId, long menuItemId) {
		cartDaoCollectionImpl.addCartItem(userId, menuItemId);
	}
	
	public List<MenuItem> getAllCartItems(Long userId) throws CartEmptyException {
		return cartDaoCollectionImpl.getAllCartItems(userId);
	}
	
	public void deleteCartItem(Long userId, Long menuItemId) {
		cartDaoCollectionImpl.removeCartItem(userId, menuItemId);
	}
}
