package com.cognizant.moviecruiser.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.moviecruiser.dao.FavoritesDao;
import com.cognizant.moviecruiser.dao.FavoritesEmptyException;
import com.cognizant.moviecruiser.model.Movie;

@Service
public class FavoriteService {

	@Autowired
	FavoritesDao favoritesDaoCollectionImpl;
	
	public List<Movie> favouriteMovies(long userId) throws FavoritesEmptyException {
		return favoritesDaoCollectionImpl.getAllFavoritesMovies(userId);
	}
	
	public void removeMovieFromFavourite(long userId, int movieId) {
		favoritesDaoCollectionImpl.removeFavoritesMovie(userId, movieId);
	}
	
	public void addMovieToFavourite(long userId, int movieId) {
		favoritesDaoCollectionImpl.addMovieToFavourite(userId, movieId);
	}
}
