package com.cognizant.moviecruiser.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.cognizant.moviecruiser.model.Favorites;
import com.cognizant.moviecruiser.model.Movie;
import com.cognizant.moviecruiser.service.MovieService;

@Component
public class FavoritesDaoCollectionImpl implements FavoritesDao {

	private static HashMap<Long, Favorites> userFavorites;

	public FavoritesDaoCollectionImpl() {
		super();
		if (userFavorites == null) {
			userFavorites = new HashMap<Long, Favorites>();
		}
	}
	
	@PostConstruct
	public void init() {
		MovieDaoCollectionImpl movieDaoCollectionImpl = new MovieDaoCollectionImpl();
		
		Favorites favorites = new Favorites((ArrayList<Movie>) movieDaoCollectionImpl.getMovieListCustomer(), 100);
		userFavorites.put(1l, favorites);
	}

	@Override
	public List<Movie> getAllFavoritesMovies(long userId) throws FavoritesEmptyException {
		// TODO Auto-generated method stub
		if (userFavorites.isEmpty()) {
			throw new FavoritesEmptyException();
		}
		Favorites favorites = userFavorites.get(userId);
		return favorites.getFavouriteMovies();
	}

	@Override
	public void removeFavoritesMovie(long userId, int movieId) {
		Favorites favorites = userFavorites.get(userId);
		ListIterator<Movie> iterator = favorites.getFavouriteMovies().listIterator();
		while (iterator.hasNext()) {
			if (iterator.next().getId() == movieId) {
				iterator.remove();
				break;
			}
		}
	}

	@Override
	public void addMovieToFavourite(long userId, int movieId) {
		// TODO Auto-generated method stub
		MovieService movieService = new MovieService();
		Movie movie = movieService.getMovieById(movieId);
		Favorites favorites = userFavorites.get(userId);
		if (favorites != null) {
			ArrayList<Movie> favouriteMovies = favorites.getFavouriteMovies();
			favouriteMovies.add(movie);
		} else {
			favorites = new Favorites();
			ArrayList<Movie> favouriteMovies = new ArrayList<>();
			favouriteMovies.add(movie);
			userFavorites.put(userId, favorites);
		}
	}

}
