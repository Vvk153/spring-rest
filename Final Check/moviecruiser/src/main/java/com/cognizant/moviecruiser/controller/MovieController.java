package com.cognizant.moviecruiser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.moviecruiser.model.Movie;
import com.cognizant.moviecruiser.service.MovieService;

@RestController
@RequestMapping("/movies")
public class MovieController {

	@Autowired
	MovieService movieService;
	
	@RequestMapping("/admin")
	public List<Movie> getAllMovieListAdmin() {
		return movieService.listOfMovieForAdmin();
	}
	
	@RequestMapping
	public List<Movie> getAllMovieListCustomer() {
		return movieService.listOfMovieForCustomer();
	}
	
	@PutMapping
	public void editMovieDetails(@RequestBody Movie movie) {
		movieService.editMovieDetails(movie);
	}
	
}
