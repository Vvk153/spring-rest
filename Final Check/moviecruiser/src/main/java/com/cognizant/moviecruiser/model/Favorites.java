package com.cognizant.moviecruiser.model;

import java.util.ArrayList;

public class Favorites {
	private ArrayList<Movie> favouriteMovies;
	
	private int totalFavouriteCount;

	public Favorites(ArrayList<Movie> favouriteMovies, int totalFavouriteCount) {
		super();
		this.favouriteMovies = favouriteMovies;
		this.totalFavouriteCount = totalFavouriteCount;
	}

	public Favorites() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void count() {
		totalFavouriteCount = favouriteMovies.size();
	}
	
	public ArrayList<Movie> getFavouriteMovies() {
		return favouriteMovies;
	}

	public void setFavouriteMovies(ArrayList<Movie> favouriteMovies) {
		this.favouriteMovies = favouriteMovies;
	}

	public int getTotalFavouriteCount() {
		return totalFavouriteCount;
	}

	public void setTotalFavouriteCount(int totalFavouriteCount) {
		this.totalFavouriteCount = totalFavouriteCount;
	}

	@Override
	public String toString() {
		return "Favorites [favouriteMovies=" + favouriteMovies + ", totalFavouriteCount=" + totalFavouriteCount + "]";
	}
	
	
}
