package com.cognizant.moviecruiser.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PutMapping;

import com.cognizant.moviecruiser.dao.MovieDao;
import com.cognizant.moviecruiser.model.Movie;

@Service
public class MovieService {

	@Autowired
	MovieDao movieDaoCollectionImpl;
	
	public List<Movie> listOfMovieForAdmin() {
		return movieDaoCollectionImpl.getMovieListAdmin();
	}
	
	public List<Movie> listOfMovieForCustomer() {
		return movieDaoCollectionImpl.getMovieListCustomer();
	}
	
	public void editMovieDetails(Movie movie) {
		movieDaoCollectionImpl.modifyMovie(movie);
	}
	
	public Movie getMovieById(long movieId) {
		return movieDaoCollectionImpl.getMovie(movieId);
	}
}
