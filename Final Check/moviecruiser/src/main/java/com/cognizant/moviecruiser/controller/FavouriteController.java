package com.cognizant.moviecruiser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.moviecruiser.dao.FavoritesEmptyException;
import com.cognizant.moviecruiser.model.Movie;
import com.cognizant.moviecruiser.service.FavoriteService;

@RestController
@RequestMapping("/favourities")
public class FavouriteController {

	@Autowired
	FavoriteService favoriteService;
	
	@GetMapping("{/id}")
	public List<Movie> listOfMoviesInFavourite(@PathVariable Long id) throws FavoritesEmptyException {
		return favoriteService.favouriteMovies(id);
	}
	
	@PostMapping("{id}/{movieId}")
	public void addMovieToFavourite(@PathVariable Long id, @PathVariable int movieId) {
		favoriteService.addMovieToFavourite(id, movieId);
	}
	
	@DeleteMapping("{id}/{movieId}")
	public void removeMovieFromFavourite(@PathVariable Long id, @PathVariable int movieId) {
		favoriteService.removeMovieFromFavourite(id, movieId);
	}
}
